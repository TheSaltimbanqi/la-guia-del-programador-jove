Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-13T20:04:56+01:00

====== Index ======

Ets jove i necesites una guia per saber tot el que cal en la teva vida laboral?
Aquí trobaras tot el que necesitis saber:
	1. [[+Contractes|Contractes]]
	2. [[+Jornada Laboral|Jornada Laboral]]
	3. [[+Ocurrencies durant un contracte|Ocurrencies durant un contracte]]
	4. [[+Seguretat Social|Seguretat Social]]


Més informació en:
	[[http://www.avalot-proves.net/wp/download/Recursos/guia_jove_supervivencia_laboral_2015_web.pdf|Guia de la supervivència jove laboral]]

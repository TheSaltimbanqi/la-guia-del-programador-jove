Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-13T20:05:36+01:00

====== Què necessitem ======

=== Capacitat per a ser contractat ===
* Major de 18 anys
* Menor de 18 legalment emancipat
* Major de 16 i menor de 18 independent o amb autorització dels tutors legals
* Estrangers amb permís de treball i residència no comunitaris

**Els menors d'edat no poden fer:**
* Treballs nocturns
* Treballs perillosos i insalubres
* Hores extraordinàries

=== Capacitat per a contractar ===
* **Persona jurídica**: 
	* A través del representant legal
* **Persona física**:
	* Major d'edat
	* Menor a través de representant legal
	* Menor emancipat major de 16 anys o amb autorització 

=== Continguts mínims d'un contracte laboral ===
S'ha de resepctar un contingut mínim que es comunicarà al treballador, fins i tot en els verbals:
**Clàusules contractuals:**
* Permanència
* No competència
* Plena dedicació
* Confidencialitat

**Continguts**:
* Període de prova:
	* Temps determinat que l'empresari i treballador poder fixar per resoldre el contracte sense causa
	* S'ha de fixar per escrit
	* Durarà el que estableixi el Conveni Col·lectiu, o, sinó 6 mesos per a tècnics titulats i 2 per als demés
	* Es tenen els mateixos drets que qualsevol altre treballador, llevat de la possibilitat de resoldre el contracte sense rebre indemnització
* Duració del contracte:
	* Els contractes poden ser de durada determinada o indefinida
	* Es presumeixen indefinits:
		* Els orals (llevat que s'acrediti la temporalitat del mateix)
		* Els realitzats en frau de llei
		* Quan no es dóna d'alta al treballador a la Seguretat Social superat el període de prova
		* Es continua treballant al finalitzar un contracte


Ìndex: [[Index]]
Contractes: [[Contractes]]

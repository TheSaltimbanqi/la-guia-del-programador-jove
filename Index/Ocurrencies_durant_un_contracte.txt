Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-13T20:22:09+01:00

====== Ocurrencies durant un contracte ======

Jove, tingues present que durant el teu contracte laboral poden succeir una serie de de moments que poden aterar la teva estancia al treball.
**Aquestes són:**
	1. [[+Un canvi|Un canvi]]
	2. [[+Una parada|Una parada]]
	3. [[+Hem de marxar|Hem de marxar]]
	4. [[+Treball acabat|Treball acabat]]

Ìndex: [[Index]]

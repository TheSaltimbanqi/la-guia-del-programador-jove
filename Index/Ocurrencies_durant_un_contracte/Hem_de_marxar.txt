Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-13T20:22:36+01:00

====== Hem de marxar ======

Quan un treballador ha de marxar i deixar el seu lloc de treball, pot ser per diferents motius:
|       Exedència       | Condicions                                                                                                                                |
|:---------------------:|:------------------------------------------------------------------------------------------------------------------------------------------|
|        forçosa        | Es conserva la antiguitat i es reservarà el teu lloc de treball.onible.                                                                   |
| per cura de familiars | Durarà un any i serà com la forçosa.                                                                                                      |
|   per cura de fill    | El primer any serà com la forçosa, fins el 3r any, on et reservaran un lloc de treball similar o igual.                                   |
|      voluntària       | No conservaras l'antiguitat del teu lloc de treball i nomès tindras dret a tornar en cas d'un lloc de treball igual o similar disponible. |



Ìndex: [[Index]]
Ocurrencies durant el contracte: [[Ocurrencies durant un contracte]]
